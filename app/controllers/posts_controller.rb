class PostsController < ApplicationController
  
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @comments = Comment.where(["post_id = ?", @post.id])
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Writing Comment Create (Add Comment Button) functionality here
  def createComment
    puts "Creating comment"
    # Instantiate a new object using form parameters
    @comment = Comment.new(comment_params)
    # Save the object
    if @comment.save
      # If save succeeds, redirect to the index action
      flash[:notice] = "Comment created successfully."
      redirect_to(:action => 'show', :id => @comment.post_id)
    else
      # If save fails, redisplay the form so user can fix problems
      flash[:notice] = "Comment is not created. Please try again."
      redirect_to(:action => 'show', :id => @comment.post_id, :errors => @comment.errors)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:title, :body, :createdby)
    end

    def comment_params
      # same as using "params[:comment]", except that it:
      # - raises an error if :comment is not present
      # - allows listed attributes to be mass-assigned
      params.require(:comment).permit(:post_id, :body)
    end
end
