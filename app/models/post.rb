class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
  belongs_to :editors, :class_name => "User"

    validates :title, presence: true
#    validates :title, presence: {message: "Title is not valid, post is not saved"}
    validates :body, presence: true
#    validates :body, presence: {message: "Body is not valid, post is not saved"}
end
