class AddCreatedByColumnToComments < ActiveRecord::Migration
  def up
    add_column("comments", "createdby", :string, :limit => 25)
  end

  def down
    remove_column("comments", "createdby")
  end
end
