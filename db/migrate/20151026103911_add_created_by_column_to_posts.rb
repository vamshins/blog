class AddCreatedByColumnToPosts < ActiveRecord::Migration
  def up
    add_column("posts", "createdby", :string, :limit => 25)
  end

  def down
    remove_column("posts", "createdby")
  end
end
